#ifndef WRAP_CONTAINERS_
#define WRAP_CONTAINERS_
#include <iostream>
#include <vector> 
#include <list>

void
take_vector_int(std::vector<int > const & int_list);

void
take_list_int(std::list<int > const & int_list);
    
void
take_vector_dd(std::vector<double > const & dd_list);   

#endif 
 

    
          

