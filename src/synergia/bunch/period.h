#ifndef PERIOD_H_
#define PERIOD_H_

#include "synergia/bunch/bunch.h"
void apply_longitudinal_periodicity (Bunch & bunch, double length);
void apply_zcut (Bunch & bunch, double length);
#endif /* PERIOD_H_ */
