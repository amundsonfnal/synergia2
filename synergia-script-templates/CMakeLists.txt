install(FILES
    cleanup
    job_example
    resumejob_example
    multijob_example_header
    multijob_example_subjob
    multijob_example_footer
    resumemultijob_example_header
    resumemultijob_example_subjob
    resumemultijob_example_footer
    job_example_pbs
    job_example_wilson
    job_tev
    job_example_hopper
    local_opts_hopper.py
    DESTINATION lib/synergia-script-templates)
