.. Synergia2 documentation master file, created by sphinx-quickstart on Tue Jun 23 13:50:50 2009.

Synergia2 documentation
=====================================

Contents:

.. toctree::
	:maxdepth: 2

	overview
	examples
	units
	code_conventions
	tools
	utils
	foundation
	bunch
        Stepper
	lattice
	simulation
	collective
	CHEF

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

