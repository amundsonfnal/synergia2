simulation module
=================
The simulation module contains the classes necessary to perform a simulation
of a Bunch traversing a Lattice.

---------
Classes
---------

.. doxygenclass:: Chef_propagator
    :project: synergia

.. doxygenclass:: Fast_mapping_term
    :project: synergia

.. doxygenclass:: Fast_mapping
    :project: synergia

.. doxygenclass:: Independent_operation
    :project: synergia

.. doxygenclass:: Fast_mapping_operation
    :project: synergia

.. doxygenclass:: Chef_propagate_operation
    :project: synergia

.. doxygenclass:: Lattice_simulator
    :project: synergia

.. doxygenclass:: Operation_extractor
    :project: synergia

.. doxygenclass:: Chef_map_operation_extractor
    :project: synergia

.. doxygenclass:: Chef_propagate_operation_extractor
    :project: synergia

.. doxygenclass:: Chef_mixed_operation_extractor
    :project: synergia

.. doxygenclass:: Operation_extractor_map
    :project: synergia

.. doxygenclass:: Operator
    :project: synergia

.. doxygenclass:: Collective_operator
    :project: synergia

.. doxygenclass:: Independent_operator
    :project: synergia

.. doxygenclass:: Propagator
    :project: synergia

.. doxygenclass:: Step
    :project: synergia

.. doxygenclass:: Stepper
    :project: synergia

.. doxygenclass:: Independent_stepper
    :project: synergia

.. doxygenclass:: Independent_stepper_elements
    :project: synergia

.. doxygenclass:: Split_operator_stepper
    :project: synergia

.. doxygenclass:: Split_operator_stepper_elements
    :project: synergia

---------
Typedefs
---------

.. doxygentypedef:: Fast_mapping_terms
    :project: synergia

.. doxygentypedef:: Independent_operation_sptr
    :project: synergia

.. doxygentypedef:: Independent_operations
    :project: synergia

.. doxygentypedef:: Fast_mapping_operation_sptr
    :project: synergia

.. doxygentypedef:: Operation_extractor_sptr
    :project: synergia

.. doxygentypedef:: Operation_extractor_map_sptr
    :project: synergia

.. doxygentypedef:: Operator_sptr
    :project: synergia

.. doxygentypedef:: Operators
    :project: synergia

.. doxygentypedef:: Collective_operator_sptr
    :project: synergia

.. doxygentypedef:: Collective_operators
    :project: synergia

.. doxygentypedef:: Independent_operator_sptr
    :project: synergia

.. doxygentypedef:: Independent_operators
    :project: synergia

.. doxygentypedef:: Step_sptr
    :project: synergia

.. doxygentypedef:: Steps
    :project: synergia

.. doxygentypedef:: Stepper_sptr
    :project: synergia

.. doxygentypedef:: Split_operator_stepper_sptr
    :project: synergia

